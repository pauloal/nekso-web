Feature:
  As a new Nekso driver 
  I want to be able to signup on Nekso site 
  So that I can have a driver account
  
  Scenario: As a new driver I want to signup in web and ensure that my information is properly validated

    Given I go to signup driver page
    When I signup as a new driver
    Then I have successfully created a driver account


