Given(/^I go to signup driver page$/) do
  SignupDriverPage.goto
end

When(/^I signup as a new driver$/) do
  SignupDriverPage.on do |page|
    page.enter_driver_account_information
    page.enter_driver_car_information
    page.click_create_account
  end
end

Then(/^I have successfully created a driver account$/) do
  pending # Write code here that turns the phrase above into concrete actions
end
