require 'yaml'

module EnvConfig
  def self.load environment
    YAML.load_file(File.join(File.dirname(__FILE__), '..', 'config', 'env.yml'))[environment]
  end
end

module DataConfig
  def self.load environment
    YAML.load_file(File.join(File.dirname(__FILE__), '..', 'config', 'data.yml'))[environment]
  end
end
