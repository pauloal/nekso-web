class CarDetails < Struct.new(:make,
    :model,
    :color,
    :year,
    :plate)
end