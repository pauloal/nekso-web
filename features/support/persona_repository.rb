class PersonaRepository

  @@map = {}

  def self.store_current_persona(personal_details)
    @@current = personal_details
  end

  def self.fetch_current_persona
    @@current
  end

  def self.store_persona(label, personal_details)
    @@map[label] = personal_details
  end

  def self.fetch_persona(label)
    @@map[label]
  end
end
