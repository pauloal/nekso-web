module IsHomePage
  def base_url
    $ENV_CONFIG["nekso"]["base_url"]
  end
end

module IsWebAdminPage
  def base_url
    $ENV_CONFIG["web_admin"]["base_url"]
  end
end
