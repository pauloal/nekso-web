require 'capybara'
require 'capybara/cucumber'
require 'capybara/rspec'

# Load Configuration
require_relative "../lib/config_reader"

$DEBUG=false

# load configs
$TEST_ENV    = ENV.fetch('TEST_ENV','qa')
$ENV_CONFIG  = EnvConfig.load($TEST_ENV)
$DATA_CONFIG = DataConfig.load('all')
$DATA_CONFIG_ENV = DataConfig.load($TEST_ENV) || puts("WARNING: No data config for current environment")

puts  <<CONFIG
#---------
# Configs
#---------
-> ENV
#$TEST_ENV

-> ENV CONFIGS
#$ENV_CONFIG

-> DATA CONFIGS (ALL)
#$DATA_CONFIG

-> DATA CONFIGS (ENV SPECIFIC)
#$DATA_CONFIG_ENV

CONFIG

# Create runtime global object to store test information
$RUNTIME = {}

VALID_BROWSERS = %w{safari chrome firefox phantomjs}

def start_session
  browser = ENV.fetch('IN_BROWSER','chrome')

  unless VALID_BROWSERS.include?(browser)
    raise "Invalid browser value set. Valid values: #{VALID_BROWSERS.join(" ")}"
  end

  require 'selenium/webdriver'
  if RUBY_PLATFORM =~ /darwin/
    Selenium::WebDriver::PhantomJS.path = File.expand_path( "../../../tools/phantomjs-osx", __FILE__ )
    Selenium::WebDriver::Chrome.driver_path = File.expand_path( "../../../tools/osx/chromedriver", __FILE__ )
  else
    # assume we're on linux, cos no one likes Microsoft, not even the employees of Microsoft.

    #Selenium::WebDriver::PhantomJS.path = File.expand_path( "../../../tools/linux/bin/phantomjs", __FILE__ )
    Selenium::WebDriver::Chrome.driver_path = File.expand_path( "../../../tools/linux/chromedriver/chromedriver", __FILE__ )
  end

  if 'phantomjs' == browser
    Capybara.register_driver :ghostdriver do |app|
      Capybara::Selenium::Driver.new(app, :browser => :phantomjs)
    end
    Capybara.default_driver    = :ghostdriver
    Capybara.javascript_driver = :ghostdriver
  else
    Capybara.default_driver = :selenium
    Capybara.register_driver :selenium do |app|
      Capybara::Selenium::Driver.new(app, :browser => browser.to_sym)
    end
  end
end

# Start browser session type
start_session

Capybara.current_session.driver.browser.manage.window.resize_to(1152, 2000) if Capybara.current_session.driver.browser.respond_to? 'manage'

# Set default
Capybara.default_max_wait_time = $ENV_CONFIG['capybara']['default_max_wait_time'] rescue 15

puts "Using driver: "+ Capybara.current_driver.to_s
