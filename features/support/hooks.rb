require 'pathname'

def artifacts_dir
  Pathname.new(ENV.fetch('ARTIFACTS_DIR', './'))
end

def screenshot_dir
  artifacts_dir.join('screenshots')
end

def create_screenshot_dir_if_necessary
  screenshot_dir.mkpath
end

def screenshot_path_for_scenario(scenario)
  screenshot_filename = "screenshot_#{scenario.name.gsub(' ', '_').gsub(/[^0-9A-Za-z_]/, '')}-#{Time.new.strftime("%Y%m%d%H%M%S")}#{rand(10**10)}.png"
  screenshot_dir.join(screenshot_filename)
end

def record_screenshot_for_scenario(scenario)
  screenshot_path = screenshot_path_for_scenario(scenario)
  create_screenshot_dir_if_necessary

  save_screenshot screenshot_path.to_s
  embed(screenshot_path.relative_path_from(artifacts_dir).to_s, 'image/png')
end

def setup_default_email_and_password_if_needed
  $RUNTIME['email'] ||= test_user_email
  $RUNTIME['password'] ||= test_user_password
end

def set_email_and_password_to_use_next_time_we_log_in(opts)
  $RUNTIME['email'] = opts.fetch(:email)
  $RUNTIME['password'] = opts.fetch(:password)
end

def email_to_login_with
  $RUNTIME.fetch('email')
end

def password_to_login_with
  $RUNTIME.fetch('password')
end

def test_user_email
  $DATA_CONFIG['web_admin_test_user']['email']
end

def test_user_password
  $DATA_CONFIG['web_admin_test_user']['password']
end
###########################################################################
# Before
###########################################################################
Before do
  #setup_default_email_and_password_if_needed

  # Set expected build version if present
  $RUNTIME['build_number'] = ENV['BUILD_NUMBER']

  # Set Ruby Admin username password from env., if present, otherwise default to qa accounts
  ENV['ADMIN_USER_NAME'] ? $RUNTIME['ADMIN_USER_NAME'] = ENV['ADMIN_USER_NAME'] : $RUNTIME['ADMIN_USER_NAME'] = '1'
  ENV['ADMIN_USER_PASSWORD'] ? $RUNTIME['ADMIN_USER_PASSWORD'] = ENV['ADMIN_USER_PASSWORD'] : $RUNTIME['ADMIN_USER_PASSWORD'] = '1'
  # Set test environment
  $RUNTIME['TEST_ENV'] = $TEST_ENV
end

###########################################################################
# After
###########################################################################

# Take screenshot when scenario fails
After do |scenario|
  if scenario.failed?
    record_screenshot_for_scenario(scenario)
  end
end
