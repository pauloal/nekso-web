module SomeRandom
  def self.string(length=8)
    (0...length).map { (97 + rand(26)).chr }.join
  end

  def self.email
    email_prefix = ENV.fetch('TEST_EMAIL_PREFIX','qa').strip
    "#{email_prefix}+#{string}_test@grr.la"
  end

  def self.phone_number
    number 10
  end

  def self.number length
    raise "number length cannot be zero!" unless length > 0
    prng = Random.new
    prng.rand(1...10).to_s + (0...length - 1).map{prng.rand(10)}.join
  end

  def self.address
    "#{number 3} #{string} #{%w[ave road street].sample}"
  end

  def self.numberBetween(int_a, int_b)
    [*int_a..int_b].sample
  end
end

module SomeValid

  def self.day
    [*1..28].sample  #below 28 for safety reasons
  end

  def self.month
    %w[January February March April May June July August September October November December].sample
  end

  def self.ven_city
    ["Caracas", "Coro", "La Vela de Coro", "Porlamar", "Valencia"].sample
  end

  def self.country_code
    ["(+58) Venezuela"].sample
  end

  def self.color
    %w[Black Blue Gold Coral Orange Pink White Yellow].sample
  end


end

module Constants
  def self.test_password
    test_password = 'qqqqqq'
  end
end
