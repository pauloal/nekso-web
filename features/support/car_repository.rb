class CarRepository
    
      @@map = {}
    
      def self.store_current_car(car_details)
        @@current = car_details
      end
    
      def self.fetch_current_car
        @@current
      end
    
      def self.store_car(label, car_details)
        @@map[label] = car_details
      end
    
      def self.fetch_car(label)
        @@map[label]
      end
    end
    