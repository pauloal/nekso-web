class BasePage
  include Capybara::DSL
  include RSpec::Matchers

  def self.goto
    page = new
    page.goto
    if block_given?
      yield page
    else
      page
    end
  end

  def self.on
    page = new
    page.verify_on_page
    yield page
  end

  def goto
    visit(url)
  end

  def page_name
    self.class.name
  end

  def identifier
    nil
  end

  def verify_on_page
    error_message = "Not on the expected page #{page_name}"

    wait_for_document_ready
    wait_for_ajax
    if identifier
      begin
        expect(page).to have_css(identifier)
      rescue Capybara::ElementNotFound
        raise "#{error_message}\n" +
                  "Can't find the page's identifier #{identifier}"
      end
    elsif !Regexp.new(url_pattern).match(current_url)
      raise "#{error_message}\n" +
                "Expected: #{url_pattern}\n" +
                "Got: #{current_url}"
    end
  end

  def path
    raise 'page object should define path'
  end

  def url
    full_url_for(path)
  end

  def path_pattern
    path
  end

  def url_pattern
    full_url_for(path_pattern)
  end

  def full_url_for(path)
    base_url + path
  end

  def set_text_field locator, value
    fill_in(locator, :with => value)
  end

  def set_text_field_by_id id, value
    find(:id, id).set value
  end


  def select_item_by_value locator, value
    within(:id, "#{locator}") do
      find("option[value='#{value}']").click
    end
  end


  def select_item locator, option
    select(option, :from => locator)
  end

  def check_checkbox locator
    find(:id, locator).set(true)
  end

  def uncheck_checkbox locator
    find(:id, locator).set(false)
  end

  def click_continue
    click_on "Continue"
  end

  def click_review
    click_on "Review"
  end

  def click_submit
    click_on "Submit"
  end

  
  def click_on_item id
    find()
  end


  def select_random_list_item(selector)
    options = all("#{selector} li").length
    randomOptionIndex = [*1..options].sample
    option = find("#{selector} li:nth-child(#{randomOptionIndex})")
    option.click
  end

  def wait_for_ajax
    Timeout.timeout(Capybara.default_max_wait_time) do
      loop until finished_all_ajax_requests?
    end
  end

  def wait_for_document_ready
    Timeout.timeout(Capybara.default_max_wait_time) do
      loop until document_ready?
    end
  end

  def document_ready?
    page.evaluate_script("document.readyState") == "complete"
  end


  def finished_all_ajax_requests?
    if page.evaluate_script('typeof jQuery') != "undefined"
      page.evaluate_script('jQuery.active').zero?
    else
      true
    end
  end
end
