require_relative 'base_page'

class SignupDriverPage < BasePage
  include IsHomePage

  def path
    "/app/driver/#/signup"
  end

  def click_create_account
    find('button.btn.btn-success.btn-lg.btn-margin-top.ng-binding.ladda-button').click
    #click_button 'Create Account'
  end

  def error_message
    find("#signup").text
  end
#Driver Details
  def set_first_name name
    fill_in('field_firstName', :with => name)
  end

  def set_last_name name
    fill_in('field_lastName', :with => name)
  end

  def set_email email
    fill_in('field_email', :with => email)
  end

  def set_email_repeat email
    fill_in('field_emailConfirmation', :with => email)
  end

  def set_password password
    fill_in('field_password', :with => password)
  end

  def set_phone_number country, phone_number
    select_item 'field_country', country
    fill_in('field_phone', :with => phone_number)
  end

  def set_city city
    select_item 'field_city', city
  end

#Car details
  def set_make make
    select_item 'field_maker', make
  end
  
  def set_model model
    select_item 'field_car', model
  end

  def set_color color
    select_item 'field_color', color
  end

  def set_year year
    select_item 'field_year', year
  end

  def set_plate plate
    fill_in('field_plate', :with => plate)
  end

  def enter_driver_account_information
    email = SomeRandom.email
    password = Constants.test_password
    first_name = SomeRandom.string
    last_name = SomeRandom.string
    country_code = SomeValid.country_code
    phone = SomeRandom.phone_number
    city = SomeValid.ven_city

    set_first_name first_name
    set_last_name last_name
    set_email email
    set_email_repeat email
    set_password password
    set_phone_number country_code, phone
    set_city city

    personal_details = PersonalDetails.new(email,
                                           password,
                                           first_name,
                                           last_name,
                                           phone)

    PersonaRepository.store_current_persona(personal_details)
  end

  def enter_driver_car_information
    make = 'Chevrolet'
    model = 'Camaro'
    color = SomeValid.color
    year = SomeRandom.numberBetween(2000, 2016)
    plate = SomeRandom.string

    set_make make
    set_model model
    set_color color
    set_year year
    set_plate plate

    car_details = CarDetails.new(make,
                                model,
                                color,
                                year,
                                plate)

    CarRepository.store_current_car(car_details)
  end

  def verify_signup_validation_errors
    within('#signup') {
      expect(page).to have_content('Please enter your first name.')
    }
  end

  def logout
    find('.my-account').click
    find("[href='/logout']").click
  end

  def verify_on_success_page
    within('#success') {
      expect(page).to have_content('Welcome')
      expect(page).to have_css("[href='/stocks']")
    }
  end
end


