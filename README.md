# User Journey Tests

This is a Capybara/Selenium based testing project for regression testing the Nekso web application.

It is separate from the application code base to ensure it does not have any privileged access to the application's internals.
